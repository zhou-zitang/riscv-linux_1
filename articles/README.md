
# 文字成果

该目录用于存放 RISC-V Linux 内核剖析 专项活动的文字性输出成果。

## 中文排版

为了更好展示输出成果，请大家在提交成果之前查阅一下 [中文文案排版指北](https://github.com/sparanoid/chinese-copywriting-guidelines/blob/master/README.zh-Hans.md)，可参考如下案例分享：

* [直播回放：提交 PR 须知 —— 中英文排版等](https://www.cctalk.com/v/16484083661860)

## 校订须知

由于惯性思维，撰写文章后很难通过自身校订完全订正，所以大都需要有一个其他同学帮助 Review 的流程。

为了提高 Review 效率，减轻校订的工作量，约定如下：

* 提交 Pull Request 前后
    * 请务必先遵守中文排版中的约定，这块不复杂，仔细一些都可以自己校订好。
        * 提交前请 `git checkout` 出来一个新的分支，然后用 [autocorrect 工具](https://gitee.com/tinylab/riscv-linux/issues/I5E5Z9) 辅助处理一下。
    * 在排版之外，还可以更通顺，避免堆砌；让结构清晰有条理，避免混乱无章；补充权威资料和规范；清晰说明源码版本、路径甚至代码。
    * 作者提交 Pull Request 后，可以在协作群通知其他同学，可以是任何你希望邀请 Review 的同学。

* 其他人在校订过程中
    * 一般需要检查错别字、排版、链接有效性以及表述是否有误等。
    * 校订过程繁重，欢迎更多人参与校订，校订不仅可以提前阅读文章，也可以通过发现并指出文中错误来提高技术能力和文章撰写水平。

* 校订过程的原子性
    * 请作者在校订完后才开始文章的修改，以避免遗漏校订人员中途新增的 Review 意见。
    * 校订人员 Review 完后，可以在 PR 评论区或协作群联系作者说明已经完成 Review。

* 基于校订信息进行修改
    * 首次提交 Pull Request 后，不可避免的，可能会收到很多 Review 意见，请保持耐心。
    * 为避免漏掉 Review 意见而造成反复，建议逐个 “评论” 采取如下措施：
        * 对于有疑问的地方，建议联系 Reviewer 沟通确认。
        * 对于认可的意见，可以直接参照 Review 意见修改，**改完后勾选 “已解决”**；
        * 在准备 2 次 PR 之前，检查是否还有未标记过的 Review 意见。

另外，已经有大量的 [校订案例](https://gitee.com/tinylab/riscv-linux/pulls?project_id=tinylab%2Friscv-linux&sort=closed_at+desc&status=closed)，作者查阅可以避免犯同样的错误，校订人员查阅可以看看校订思路。**本文文末也汇总了部分注意事项**。

## 文件名

为了避免编码的不一致，文件名统一以 `日期-英文小写字母标题.md` 的方式命名，英文标题内如果有空格，替换为 `-` 号，例如：

```
20220308-riscv-linux-quickstart.md
```

## 图片

图片请统一存放在文章专属的 `images/article_short_name/` 目录下，并**全部用小写字母不带空格**的文件名，引用方式务必使用相对地址，而不是本地绝对地址：

```
![Image Description](images/article_short_name/my_image.jpg)
```

## 文件头说明

文件头统一放在文件的开头，置顶放置，后面紧跟着的就是一级大标题（以一个井号开头标识），即文章的题目。

新撰写的文章，统一文件头如下：

```
> Author:  678 <xxx@aaa.com><br/>
> Date:    2022/0x/0y<br/>
> Revisor: ABC <xxx@yyy.com><br/>
> Project: [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Sponsor: PLCT Lab, ISCAS
```

如有具体的提案和规划，可加上提案链接：

```
> Author:  678 <xxx@aaa.com><br/>
> Date:    2022/0x/0y<br/>
> Revisor: ABC <xxx@yyy.com><br/>
> Project: [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Proposal: [某某提案](https://gitee.com/tinylab/riscv-linux/issues/xxxx)<br/>
> Sponsor: PLCT Lab, ISCAS
```

如果文章中用到社区研发的 Linux Lab 或 Linux Lab Disk，加上这样一行说明：

```
> Environment: [Linux Lab Disk](https://tinylab.org/linux-lab-disk)
```

或

```
> Environment: [Linux Lab](https://tinylab.org/linux-lab)
```


翻译的文章，统一文件头如下：

```
> Title:      [Porting Linux to a new processor architecture, part 1: The basics](https://lwn.net/Articles/654783/)<br/>
> Author:     Joël Porquet<br/>
> Translator: 123 <xxx@zzz.com><br/>
> Date:       2022/0x/0y<br/>
> Revisor:    ABC <xxx@yyy.com><br/>
> Project:    [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Sponsor:    PLCT Lab, ISCAS
```

## 翻译格式

为了方便校订和读者交叉确认，所有翻译文章采用英文原文和中文翻译整段对照的方式，章节小标题采用 `翻译（原标题）` 的方式展示，具体请参考：

* [LWN 531148: Linux 内核文件中的非常规节](https://tinylab.org/lwn-531148/)

## 注意事项

- Pull Request
    - 如果同时撰写多篇新文章，需要为每篇文章创建一个新的分支，比如 `article0-v0`, `article1-v0`
    - 准备提交时，请 `git checkout` 出一个新的分支，比如 `article0-v0-pr`, `article-v1-pr`
        - 记得用 `git rebase` 把本地修改记录精简一些，合并多笔 commit 为一笔并精简里头的 message log，可参考 [How to Combine Multiple Git Commits into One](https://www.w3docs.com/snippets/git/how-to-combine-multiple-commits-into-one-with-3-steps.html)
          ```
          $ git checkout -b article0-v1-pr         // 每次 rebase 前务必创建一个新的分支，确保本地至少有一个备份分支，因为 rebase 可能会出错
          $ git fetch --all                        // 拉取新的社区仓库
          $ git rebase --onto origin/master --root // rebase 当前分支到社区最新的 master 分支，可消除一堆 merge 记录
          $ git rebase -i 1st_commit_id^           // 合并记录，把第 2 行开始的所有 ^pick 替换为 squash 
          ```
        - 每个 commit 的 subject title 必须要有明确的意义，用一句话而不是一个单词描述
        - commit log 必须干净整洁，需简要准确地描述 commit，仅在 commit log 末尾保留一行 `Signed-off-by`
        - 简单起见，除非本地的多笔 commit 都是干净整洁的，否则，每笔 PR 只能包含一笔 commit
    - 请务必确保每次 PR 都只包含一篇新文章，粒度越小，Review 和 Merge 越快
    - 老师 Review 过后，请务必 checkout 出一个新的分支来做修订，不要直接修改老的分支
        - 例如，checkout 出来的分支可以递增版本号，`article0-v1`, `article1-v1`
        - 再次提交 PR 时，同样需要 checkout 出新的 `-pr` 分支，例如 `article0-v1-pr` 和 `article1-v1-pr`
    - 无论是首次提交 PR，还是修订后提交 PR，请确保分支都是不同的，通过递增版本号可以保证
    - 用于 PR 的分支，在 PR 之后，不能二次更新，只能删除，通过上面 checkout 出带 `-pr` 后缀的分支可以保证
    - 下面的这张表完整描述了上述流程：

         First Version|  Pull Request         | Modify After Review |  New Pull Request ...
        --------------|-----------------------|---------------------|-----------------------
         article0-v0  | article0-v0-pr        | article0-v1         |  article0-v1-pr ...
         article1-v0  | article1-v0-pr        | article1-v1         |  article1-v1-pr ...
         article2-v0  | article2-v0-pr        | article2-v1         |  article2-v1-pr ...


- 章节标题
    - 章节标题内不要带序号
        - 成果发布渠道多样，有的渲染器会自动加序号，如果提前加了，输出内容会重复显示序号，甚至显示的序号不一样，会引起混乱
    - 不要出现没有标题的段落
        - 一般结构，要有头有尾：“前言”/“背景”、“总结”/“小结”、“参考资料”/“推荐资料” 等
    - 大标题为一级标题，以一个井号开头，例如 `# RISC-V Qemu 用法简介`
    - 二级标题以两个井号开头，例如 `## 下载 Qemu`
    - 为了方便区分标题和正文，标题内如果涉及函数等“代码”，请不要使用行内代码标识括起来

- 段落说明
    - 不要在一个独立的段落内部手动加换行，这反而会打乱编辑器的自动换行功能
    - 段落不要过长，要避免过度堆积引起难以审阅和阅读
        - 要及时另取一个新的段落，段落间需要加一个空行（相当于两个换行），而不只是加一个换行，有留白更舒适
    - 注意承前启后，增加必要的衔接词
    - 注意避免冗余用词，比如说一句话里头 4、5 个 “的”
    - 需要连贯通顺，建议通读几遍，自己都读起来别扭，就不能提交
    - 巧用 List 但是不要滥用 List
        - List 前置 `-`，`*` 等无序符号，或者 `1`, `2`, `3` 等有序符号
        - 有序 List 仅支持 `1.`, `2.`, `3.`，不支持，`1、`, `2、`, `3、`，请勿混淆 

- 章节数
    - 每篇文章的小节数不要过少，也不要过多
    - 小节过少的例子：“前言”、“正文标题”，“小结”，“参考资料”
        - 这种情况必须把 “正文标题” 拆解成多个步骤，每个步骤作为二级标题（两个井号开头），避免正文层次过深
    - 小节过多的情况
        - 一种是把太多细节放到二级标题里头，这种情况是下沉细节，到小节内陈述即可
        - 还有一种是把多种内容杂糅在一起，这种情况建议拆分成不同的文章

- 内容层级
    - 建议每篇文章的小标题层级不要超过 4 级（最多 4 个井号开头）
    - 每个 List 的层级不超过 3 级（最多缩进三次）

- 代码说明
    - 行内代码请类似这样：这个函数 `some_func()` 做了某个功能，用 ESC 下的引号括起来，引号外面加空格，引号内不要有额外空格。
    - 独立的代码前后用三个 ESC 下的引号引起来，首行标准代码所在文件和行号，例如 `// init/main.c:50`
    - 涉及命令行的请统一用 `$ ` 提示符，不建议带入完整路径，尤其是个人账号信息这些的路径，如果需要 root 权限，可以用 `# ` 提示符
    - 代码内如果需要加注释，也需要保持中英文混排风格，不要拥挤难看
    - 如果能作为纯文本粘贴，就不需要截图，包括代码以及代码运行的日志都可以直接粘贴纯文本。截图副作用比较大，费时费力费流量而且不方便 Review。

- 代码与 List 混排
    - 如果是行内的短代码，可以混排
    - 如果是多行代码，不建议与 List 混排，会导致代码缩进很难处理，阅读也很难看
    - 如果涉及多行代码，建议把 List 改回小节标题的形式，或者如果层次已经很深，可以直接用文字、代码按顺序 “平铺直叙” 的方式

- 链接说明
    - 建议把所有链接汇总在文末，以 `[1]: url1` 这样的方式一行一个，递增数字序号，相当于增加了一个简短的别名
    - 在文章内部和文末的 “参考资料” 中可以直接这样引用：`[Title1][1]`
        - 切莫这样用 ~~`[Title1](1)`~~，后面这种用法是错误的，小括号内必须是完整的 url
    - 链接前后也建议加空格，方便读者快速识别链接，例如，这里是 [泰晓科技](https://tinylab.org) 的网站地址。

- 其他杂项
    - 中英文排版中用到的空格全部是半角空格，不是全角空格，不然会太宽，显得突兀。
    - 中英文混排的时候，如果主体是中文，那么用中文符号，比如逗号（，）、句号（。）、冒号（：）这些。