# RISC-V Linux 内核及周边技术动态 

## 20220821：第 8 期

### 内核动态

* V8: [RISC-V IPI Improvements](https://lore.kernel.org/linux-riscv/20220820065446.389788-1-apatel@ventanamicro.com/)

    These patches were originally part of the "Linux RISC-V ACLINT Support" series but this now a separate series so that it can be merged independently of the "Linux RISC-V ACLINT Support" series.

* V3: [Fix RISC-V/PCI dt-schema issues with dt-schema v2022.08](https://lore.kernel.org/linux-riscv/20220819231415.3860210-1-mail@conchuod.ie/)

    Got a few fixes for PCI dt-bindings that I noticed after upgrading my dt-schema to v2022.08. Since all the dts patches are for "my" boards, I'll take them once the bindings are approved.

* GIT PULL:[RISC-V Fixes for 6.0-rc2](https://lore.kernel.org/linux-riscv/mhng-74337228-62c4-40ed-b7af-0d988ff94993@palmer-mbp2014/)

    * A fix to make the ISA extension static keys writable after init.  This manifests at least as a crash when loading modules (including KVM).
    * A fixup for a build warning related to a poorly formed comment in our perf driver.

* V3: [riscv: kexec: Fixup crash_save percpu and machine_kexec_mask_interrupts](https://lore.kernel.org/linux-riscv/20220819025444.2121315-1-guoren@kernel.org/)

    Current riscv kexec can't crash_save percpu states and disable interrupts properly. The patch series fix them, make kexec work correct.

* V1: [riscv: dma-mapping: Use conventional cache operations for dma_sync*()](https://lore.kernel.org/linux-riscv/20220818165105.99746-1-s.miroshnichenko@yadro.com/) 

    As of for_device(FROM_DEVICE), according to the inspirational discussion about matching the DMA API and cache operations, the inval or a no-op should be here, and this would resonate with most other arches. But in a later discussion it was decided that the clean is indeed preferable and safer, though slower.

* V2: [Add support for Renesas RZ/Five SoC](https://lore.kernel.org/linux-riscv/20220815151451.23293-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)

    The RZ/Five microprocessor includes a RISC-V CPU Core (AX45MP Single) 1.0 GHz, 16-bit DDR3L/DDR4 interface. And it also has many interfaces such as Gbit-Ether, CAN, and USB 2.0, making it ideal for applications such as entry-class social infrastructure gateway control and industrial gateway control.

* V6: [RISC-V: Create unique identification for SoC PMU](https://lore.kernel.org/linux-riscv/20220815132251.25702-1-nikita.shubin@maquefel.me/)

    This series aims to provide matching vendor SoC with corresponded JSON bindings. Also added SBI firmware events pretty names, as any firmware that supports SBI PMU should also support firmare events.

* V1: [RISC-V: Add fast call path of crash_kexec()](https://lore.kernel.org/linux-riscv/20220814162922.2398723-6-sashal@kernel.org/) 

    Currently, almost all archs (x86, arm64, mips...) support fast call of crash_kexec() when "regs && kexec_should_crash()" is true. But RISC-V not, it can only enter crash system via panic().

* V1: [riscv: Allwinner D1 platform support](https://lore.kernel.org/linux-riscv/20220815050815.22340-1-samuel@sholland.org/)

    This series adds the Kconfig/defconfig plumbing and devicetrees for a range of Allwinner D1-based boards. Many features are already enabled, including USB, Ethernet, and WiFi.

### 周边技术动态

#### Qemu

* V3: [target/riscv: Use official extension names for AIA CSRs](https://lore.kernel.org/qemu-devel/20220820042958.377018-1-apatel@ventanamicro.com/)

    The arch review of AIA spec is completed and we now have official extension names for AIA: Smaia (M-mode AIA CSRs) and Ssaia (S-mode AIA CSRs).

* V13: [Improve PMU support](https://lore.kernel.org/qemu-devel/20220816232321.558250-1-atishp@rivosinc.com/)

    The latest version of the SBI specification includes a Performance Monitoring Unit(PMU) extension which allows the supervisor to start/stop/configure various PMU events.

* V1: [hw/riscv: Setting address of vector reset is improved](https://lore.kernel.org/qemu-devel/20220815174138.19766-1-coder@frtk.ru/)

    Previously address is set by default value 0x1000 which is hardcoded in target/riscv/cpu_bits.h If add to new RISC-V Machine in which ROM area is not based on 0x1000 address than there is problem of running simulation

#### U-Boot

* V1: [doc: qemu-riscv: describe booting with QEMU and KVM](https://lore.kernel.org/u-boot/20220814142248.2429845-1-heinrich.schuchardt@canonical.com/)

    The ELF U-Boot image produced by qemu-riscv64_smode_defconfig can be used to boot with QEMU and KVM.

## 20220814：第 7 期

### 内核动态

* GIT PULL: [RISC-V Patches for the 5.20 Merge Window, Part 2](https://lore.kernel.org/linux-riscv/mhng-563e7d68-e504-4e0a-b666-f7c2fbab62db@palmer-ri-x1c9/)

    There's still a handful of new features in here, but there are a lot of fixes/cleanups as well:
  * Support for the Zicbom for explicit cache-block management, along with
  the necessary bits to make the non-standard cache management ops on
  the Allwinner D1 function.
  * Support for the Zihintpause extension, which codifies a go-slow
  instruction used for cpu_relax().
  * Support for the Sstc extension for supervisor-mode timer/counter
  management.
  * Many device tree fixes and cleanups, including a large set for the
  Canaan device trees.
  * A handful of fixes and cleanups for the PMU driver.

* V3: [Support RISCV64 arch and common commands](https://lore.kernel.org/linux-riscv/20220813031753.3097720-1-xianting.tian@linux.alibaba.com/)

    This series of patches are for Crash-utility tool, it make crash tool support RISCV64 arch and the common commands(*, bt, p, rd, mod, log, set, struct, task, dis, help -r, help -m, and so on).

* V1: [riscv: enable CD-ROM file systems in defconfig](https://lore.kernel.org/linux-riscv/20220812200853.311474-1-heinrich.schuchardt@canonical.com/)

    CD-ROM images are still commonly used for installer images and other data exchange. These file systems should be supported on RISC-V by default like they are on x86_64.

* V2: [RISC-V: Clean up the Zicbom block size probing](https://lore.kernel.org/linux-riscv/20220812154010.18280-1-palmer@rivosinc.com/)

    This fixes two issues: I truncated the warning's hart ID when porting to the 64-bit hart ID code, and the original code's warning handling could fire on an uninitialized hart ID.

* V3: [Risc-V Svinval support](https://lore.kernel.org/linux-riscv/20220812042921.14508-1-mchitale@ventanamicro.com/)

    This patch adds support for the Svinval extension as defined in the Risc V Privileged specification.

* V1: [Fix RISC-V/PCI dt-schema issues with dt-schema v2022.08](https://lore.kernel.org/linux-riscv/20220811203306.179744-1-mail@conchuod.ie/)

    Got a few fixes for PCI dt-bindings that I noticed after upgrading my dt-schema to v2022.08. I am unsure if some of these patches are the right fixes, which I noted in the patches themselves, especially the address translation property.

* V6: [RISC-V fixups to work with crash tool](https://lore.kernel.org/linux-riscv/20220811074150.3020189-1-xianting.tian@linux.alibaba.com/)

    This patch set just put these patches together, and with three new patch 4, 5, 6. these six patches are the fixups for machine_kexec, kernel mode PC for vmcore and improvements for vmcoreinfo, memory layout dump and fixup schedule out issue in machine_crash_shutdown().

* V1: [wireguard: selftests: set CONFIG_NONPORTABLE on riscv32](https://lore.kernel.org/linux-riscv/20220809145757.83673-1-Jason@zx2c4.com/)

    When the CONFIG_PORTABLE/CONFIG_NONPORTABLE switches were added, various configs were updated, but the wireguard config was forgotten about. This leads to unbootable test kernels, causing CI fails.

* V9: [arch: Add qspinlock support and atomic cleanup](https://lore.kernel.org/linux-riscv/20220808071318.3335746-1-guoren@kernel.org/)

    RISC-V LR/SC pairs could provide a strong/weak forward guarantee that depends on micro-architecture. And RISC-V ISA spec has given out several limitations to let hardware support strict forward guarantee.

### 周边技术动态

#### Qemu

* V2: [riscv: Make semihosting configurable for all privilege modes](https://lore.kernel.org/qemu-devel/CA+tJHD4Fdv54_u9vffu9tNuor4Tu_Ld-eYZkLRmTxi=X2wknnw@mail.gmail.com/)

    Unlike ARM, RISC-V does not define a separate breakpoint type for semihosting. Instead, it is entirely ABI. However, RISC-V debug specification provides ebreak{m,s,u,vs,vu} configuration bits to allow ebreak behavior to be configured to trap into debug mode instead.

* V3: [QEMU: Fix RISC-V virt & spike machines' dtbs](https://lore.kernel.org/qemu-devel/20220810184612.157317-1-mail@conchuod.ie/)

    The device trees produced automatically for the virt and spike machines fail dt-validate on several grounds. Some of these need to be fixed in the linux kernel's dt-bindings, but others are caused by bugs in QEMU.
    
* V9: [Implement Sstc extension](https://lore.kernel.org/qemu-devel/20220810184548.3620153-1-atishp@rivosinc.com/)

    This series implements Sstc extension[1] which was ratified recently. The first patch is a prepartory patches while PATCH 2 adds stimecmp support while PATCH 3 adds vstimecmp support. This series is based on top of upstream commit (faee5441a038).

* V8: [RISC-V Smstateen support](https://lore.kernel.org/qemu-devel/20220809041643.124888-1-mchitale@ventanamicro.com/)

    This series adds support for the Smstateen specification which provides a mechanism to plug the potential covert channels which are opened by extensions that add to processor state that may not get context-switched.

#### Buildroot

* GIT COMMIT: [board/riscv/nommu: bump kernel version and drop no longer needed patch](https://lore.kernel.org/buildroot/20220811202816.E96F487348@busybox.osuosl.org/)

    Bump the kernel version for all riscv nommu configs from 5.18 to 5.19. That way, we can remove the one and only riscv nommu patch, since this patch is included in kernel 5.19.

## 20220807：第 6 期

### 内核动态

* GIT PULL: [RISC-V Patches for the 5.20 Merge Window, Part 1](https://lore.kernel.org/linux-riscv/mhng-1cbba637-6dd2-456a-859b-9d3f8be6bab7@palmer-mbp2014/)

    There are also a handful of cleanups and improvements
    * Enabling the FPU is now a static_key.
    * Improvements to the Svpbmt support.
    * CPU topology bindings for a handful of systems.
    * Support for systems with 64-bit hart IDs.
    * Many settings have been enabled in the defconfig, including both support for the StarFive systems and many of the Docker requirements.

* V1: [QEMU: Fix RISC-V virt & spike machines' dtbs](https://lore.kernel.org/linux-riscv/20220805155405.1504081-1-mail@conchuod.ie)

    The device trees produced automatically for the virt and spike machines fail dt-validate on several grounds. Some of these need to be fixed in the linux kernel's dt-bindings, but others are caused by bugs in QEMU

* V5: [RISC-V fixups to work with crash tool](https://lore.kernel.org/linux-riscv/20220802121818.2201268-1-xianting.tian@linux.alibaba.com/)

    This patch set just put these patches together, and with three new patch 4, 5, 6. these six patches are the fixups for machine_kexec, kernel mode PC for vmcore and improvements for vmcoreinfo, memory layout dump and fixup schedule out issue in machine_crash_shutdown().

* V2: [Support RISCV64 arch and common commands](https://lore.kernel.org/linux-riscv/20220801043040.2003264-1-xianting.tian@linux.alibaba.com/)

    This series of patches are for Crash-utility tool, it make crash tool support RISCV64 arch and the common commands(*, bt, p, rd, mod, log, set, struct, task, dis, help -r, help -m, and so on).

* V1: [DT schema warnings on Risc-V virt machine](https://lore.kernel.org/linux-riscv/20220803170552.GA2250266-robh@kernel.org/)

    There could also be other warnings from non-default configurations.

### 周边技术动态

#### Qemu

* V1: [QEMU: Fix RISC-V virt & spike machines' dtbs](https://lore.kernel.org/qemu-devel/20220805155405.1504081-1-mail@conchuod.ie/)

    The device trees produced automatically for the virt and spike machines fail dt-validate on several grounds. Some of these need to be fixed in the linux kernel's dt-bindings, but others are caused by bugs in QEMU.
    
* V8: [Implement Sstc extension](https://lore.kernel.org/qemu-devel/20220804014240.2514957-1-atishp@rivosinc.com/)

    This series implements Sstc extension[1] which was ratified recently. The first patch is a prepartory patches while PATCH 2 adds stimecmp support while PATCH 3 adds vstimecmp support. 

* V7: [RISC-V Smstateen support](https://lore.kernel.org/qemu-devel/20220801171843.72986-1-mchitale@ventanamicro.com/)

    This series adds support for the Smstateen specification which provides a mechanism plug potential covert channels which are opened by extensions that add to processor state that may not get context-switched.

#### Buildroot

* V1: [package/llvm: Support for RISC-V on the LLVM package](https://lore.kernel.org/buildroot/20220801215433.1927D86C6D@busybox.osuosl.org/)

    The initial support for the LLVM package did not include RISC-V, and needed to be added. Some special casing is needed for the LLVM_TARGETS_TO_BUILD variable, which expects a RISCV value regardless of whether riscv32 or riscv64 is chosen.

## 202200731：第 5 期

### 内核动态

* GIT PULL: [A Single RISC-V Fix for 5.19](https://lore.kernel.org/linux-riscv/mhng-b20f34d5-a1cb-4254-8cc5-d3c7752b3908@palmer-mbp2014/)

    A build fix for "make vdso_install" that avoids an issue trying to install the compat VDSO. 

* GIT PULL: [KVM/riscv changes for 5.20](https://lore.kernel.org/linux-riscv/CAAhSdy2iH-WpitweQ_nCYm6p0S5S_fmQ3x37FdAe7tEmu_np0A@mail.gmail.com/)

    We have following KVM RISC-V changes for 5.20:

    - Track ISA extensions used by Guest using bitmap
    - Added system instruction emulation framework
    - Added CSR emulation framework
    - Added gfp_custom flag in struct kvm_mmu_memory_cache
    - Added G-stage ioremap() and iounmap() functions
    - Added support for Svpbmt inside Guest

* V1: [riscv: enable software resend of irqs](https://lore.kernel.org/linux-riscv/20220729111116.259146-1-conor.dooley@microchip.com/)

    The PLIC specification does not describe the interrupt pendings bits as read-write, only that they "can be read". To allow for retriggering of 
    interrupts enable HARDIRQS_SW_RESEND for RISC-V.

* V1: [doc: RISC-V: Document that misaligned accesses are supported](https://lore.kernel.org/linux-riscv/20220728210715.17214-1-palmer@rivosinc.com/)

    The RISC-V ISA manual used to mandate that misaligned accesses were supported in user mode, but that requirement was removed in 2018 via riscv-isa- 
    manual commit 61cadb9 Since the Linux uABI was already frozen at that point it's just been demoted to part of the uABI, but that was never written 
    down.

* V2: [Improve CLOCK_EVT_FEAT_C3STOP feature setting](https://lore.kernel.org/linux-riscv/20220727114302.302201-1-apatel@ventanamicro.com/) 

    This series improves the RISC-V timer driver to set CLOCK_EVT_FEAT_C3STOP feature based on RISC-V platform capabilities.

* V2: [RISC-V: Add mvendorid, marchid, and mimpid to /proc/cpuinfo output](https://lore.kernel.org/linux-riscv/20220727043829.151794-1-apatel@ventanamicro.com/)

    Identifying the underlying RISC-V implementation can be important for some of the user space applications. For example, the perf tool uses arch specific CPU implementation id (i.e. CPUID) to select a JSON file describing custom perf events on a CPU. Currently, there is no way to identify RISC-V implementation so we add mvendorid, marchid, and mimpid to /proc/cpuinfo output.

* V1: [checkstack: add riscv support for scripts/checkstack.pl](https://lore.kernel.org/linux-riscv/20220713194112.15557-1-wafgo01@gmail.com/)

    scripts/checkstack.pl lacks support for the riscv architecture. Add support to detect "addi sp,sp,-FRAME_SIZE" stack frame generation instruction

* V1: [Add support for Renesas RZ/Five SoC](https://lore.kernel.org/linux-riscv/20220726180623.1668-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)

    The RZ/Five microprocessor includes a RISC-V CPU Core (AX45MP Single) 1.0 GHz, 16-bit DDR3L/DDR4 interface. And it also has many interfaces 
    such as Gbit-Ether, CAN, and USB 2.0, making it ideal for applications such as entry-class social infrastructure gateway control and industrial 
    gateway control.

* V4: [RISC-V fixups to work with crash tool](https://lore.kernel.org/linux-riscv/20220726093729.1231867-1-xianting.tian@linux.alibaba.com/)

    This patch series just put these patches together, and with two new patch 4, 5. these five patches are the fixups for machine_kexec, 
    kernel mode PC for vmcore and improvements for vmcoreinfo and memory layout dump.

* V8: [arch: Add qspinlock support with combo style](https://lore.kernel.org/linux-riscv/20220724122517.1019187-1-guoren@kernel.org/)

    Enable qspinlock and meet the requirements mentioned in a8ad07e5240c9 RISC-V LR/SC pairs could provide a strong/weak forward guarantee 
    that depends on micro-architecture.

### 周边技术动态

#### Qemu

* V11: [Improve PMU support](https://lore.kernel.org/qemu-devel/20220727064913.1041427-1-atishp@rivosinc.com/)

    The latest version of the SBI specification includes a Performance Monitoring Unit(PMU) extension which allows the supervisor to 
    start/stop/configure various PMU events.This series implements remaining PMU infrastructure to support PMU in virt machine.

* V5: [target/riscv: Add Zihintpause support](https://lore.kernel.org/qemu-devel/20220725034728.2620750-1-daolu@rivosinc.com/)

    This patch adds RISC-V Zihintpause support. The extension is set to be enabled by default and opcode has been added to insn32.decode. 
    Added trans_pause to exit the TB and return to main loop.
    
#### Buildroot

* V2: [arch/Config.in.riscv: lp64f ABI is only supported if MMU is enabled](https://lore.kernel.org/buildroot/20220726163951.2111731-1-thomas.petazzoni@bootlin.com/)

    Even though that seems weird, the LP64F ABI is only supported when MMU support is enabled. In deed, as per commit 
    9a51381cedc16e6d70cb85e1144f6e0fa89af69a ("package/uclibc: prevent config with unsupported RISC-V float ABI"), 
    uClibc does not support LP64F. But uClibc is the only C library that support RISC-V 64-bit noMMU.

## 20220723：第 4 期

### 内核动态

* v4: [Fix RISC-V's arch-topology reporting](https://lore.kernel.org/linux-riscv/96972ad8-d146-3bc2-0e49-ffe88580bbee@microchip.com/T/#t)

    arm64's topology code basically applies to RISC-V too, so it has been made generic along with the removal of MPIDR related code, which appears to be redudant code since '3102bc0e6ac7 ("arm64: topology: Stop using MPIDR for topology information")' replaced the code that actually interacted with MPIDR with default values.
    
* v1: [Support RISCV64 arch and common commands](https://lore.kernel.org/linux-riscv/20220717042929.370022-1-xianting.tian@linux.alibaba.com/)

    This series of patches make crash tool support RISCV64 arch and the common commands(*, bt, p, rd, mod, log, set, struct, task, dis and so on). To make the crash tool work normally for RISCV64 arch, we need a Linux kernel patch(under reviewing), which exports the kernel virtual memory layout, va_bits, phys_ram_base to vmcoreinfo, it can simplify the development of crash tool.
    
* v1: [Improve CLOCK_EVT_FEAT_C3STOP feature setting](https://lore.kernel.org/linux-riscv/20220719054729.2224766-1-apatel@ventanamicro.com/)

    This series improves the RISC-V timer driver to set CLOCK_EVT_FEAT_C3STOP feature based on RISC-V platform capabilities.
    
* v1: [Fixups to work with crash tool](https://lore.kernel.org/linux-riscv/20220717101323.370245-1-xianting.tian@linux.alibaba.com/)

    his patch series just put these patches together, and with a new patch 5. these five patches are the fixups for kexec, vmcore and improvements for vmcoreinfo and memory layout dump. With these 5 patches(patch 3 is must), crash tool can work well to analyze a vmcore.
    
* v5: [Canaan devicetree fixes](https://lore.kernel.org/linux-riscv/20220705215213.1802496-1-mail@conchuod.ie/T/#t)

    This series should rid us of dtbs_check errors for the RISC-V Canaan k210 based boards. To make keeping it that way a little easier, I changed the Canaan devicetree Makefile so that it would build all of the devicetrees in the directory if SOC_CANAAN.
    
* v7: [RISC-V IPI Improvements](https://lore.kernel.org/linux-riscv/20220720152348.2889109-1-apatel@ventanamicro.com/#r)

    These patches were originally part of the "Linux RISC-V ACLINT Support" series but this now a separate series so that it can be merged independently of the "Linux RISC-V ACLINT Support" series.
    
* V3: [Support for 64bit hartid on RV64 platforms](https://lore.kernel.org/linux-riscv/mhng-4c49edf1-6367-4dd0-bec7-c6719745ecb5@palmer-mbp2014/T/#t)

    The hartid can be a 64bit value on RV64 platforms. This series updates the code so that 64bit hartid can be supported on RV64 platforms.
    
* v4: [riscv:uprobe fix SR_SPIE set/clear handling](https://lore.kernel.org/linux-riscv/20220721065820.245755-1-zouyipeng@huawei.com/T/#u)    
    
    In riscv the process of uprobe going to clear spie before exec the origin insn,and set spie after that.But When access the page which origin insn has been placed a page fault may happen and irq was disabled in arch_uprobe_pre_xol function,It cause a WARN as follows.    
    
* v1: [Add HiFive Unmatched LEDs](https://lore.kernel.org/linux-riscv/20220717110249.GF14285@duo.ucw.cz/T/#t)
    
    This series adds support for the two LEDs on the HiFive Unmatched RISC-V board.    
    
* v7: [Add Sstc extension support](https://lore.kernel.org/linux-riscv/20220722165047.519994-1-atishp@rivosinc.com/)
    
     This series implements Sstc extension support which was ratified recently. Before the Sstc extension, an SBI call is necessary to generate timer interrupts as only M-mode have access to the timecompare registers. Thus, there is significant latency to generate timer interrupts at kernel. 

### 周边技术动态

#### Qemu

* v6: [target/riscv: Add stimecmp support](https://lore.kernel.org/qemu-devel/20220722010046.343744-3-atishp@rivosinc.com/)

    stimecmp allows the supervisor mode to update stimecmp CSR directly to program the next timer interrupt. This CSR is part of the Sstc extension which was ratified recently.
    
* v1: [Fix pointer masking functionality for RISC-V](https://lore.kernel.org/qemu-devel/20220717101543.478533-1-space.monkey.delivers@gmail.com/)    
    
    This patch fixes a typo which leads to broken pointer masking functionality for RISC-V.   
    
#### Buildroot

* v4: [Fix RV64 NOMMU and add Canaan K210 SoC support](https://lore.kernel.org/buildroot/20220720024531.435748-1-damien.lemoal@opensource.wdc.com/)

    This series adds support for building 64-bits RISC-V NOMMU kernels (both bootable kernels and u-boot sdcard boot envronements) for NOMMU RISC-V 64-bits boards. The board supported include QEMU and many boards using the dual-core RISC-V 64-bits Cannan Kendryte K210 SoC.

## 20220716：第 3 期

### 内核动态

* v5: [use static key to optimize pgtable_l4_enabled](https://lore.kernel.org/linux-riscv/20220715134847.2190-1-jszhang@kernel.org/T/#t)
    
    The pgtable_l4|[l5]_enabled check sits at hot code path, performance is impacted a lot. Since pgtable_l4|[l5]_enabled isn't 
    changed after boot, so static key can be used to solve the performance issue[1].
 
* v4: [Fix RISC-V's arch-topology reporting](https://lore.kernel.org/linux-riscv/20220715175155.3567243-1-mail@conchuod.ie/T/#t)
    
    The goal here is the fix the incorrectly reported arch topology on RISC-V which seems to have been broken since it was added.
    
* next: [arch_topology: Fix cache attributes detection in the CPU hotplug pat](https://lore.kernel.org/linux-riscv/YtBX8WX+oyPww%2Fm+@arm.com/T/#t)
    
    Move the call to detect_cache_attributes() inside update_siblings_masks() to ensure the cacheinfo is updated before the LLC 
    sibling masks are updated.
    
* v3: [clear riscv dtbs_check errors](https://lore.kernel.org/linux-riscv/mhng-91bfbf9d-d8cc-4642-9688-20ef7ab21512@palmer-ri-x1c9/T/#t)

    Couple conversions from txt to yaml here with the intent of fixing the the dtbs_check warnings for riscv when building with "defconfig". 
    Atul Khare already sent patches for the gpio-line-names & cache-sets (which went awol) and will clear the remaining two errors.

* v3: [Improve vmcoreinfo and memory layout dump](https://lore.kernel.org/linux-riscv/20220714113300.367854-2-xianting.tian@linux.alibaba.com/T/#u)

    This patch series are the improvements for vmcoreinfo and memory layout dump.
    
* v2: [riscv: dts: starfive: correct number of external interrupts](https://lore.kernel.org/linux-riscv/27617ba6-addf-6f29-e1a8-2cb813dc303a@microchip.com/T/#t)

    The PLIC integrated on the Vic_U7_Core integrated on the StarFive JH7100 SoC actually supports 133 external interrupts. 
    127 of these are exposed to the outside world; the remainder are used by other devices that are part of the core-complex 
    such as the L2 cache controller.

* v5: [Microchip soft ip corePWM driver](https://lore.kernel.org/linux-riscv/20220708143923.1129928-1-conor.dooley@microchip.com/T/#t)

    The duty cycle calculation has been fixed - the problem was exactly what I suspected in my replies to your review. I had 
    to block the use of a 0xFF period_steps register value (which I think should be covered by the updated comment and limitation #2).
    
* v1: [Proof of concept for rv32 svpbmt support](https://lore.kernel.org/linux-riscv/20220705100523.1204595-1-guoren@kernel.org/T/#t)

    RISC-V 32bit also requires svpbmt in cost-down chip embedded scenarios, and their RAM is limited (No more than 1GB). It is worth 
    mentioning that rv32-Linux currently only supports 1GB of DRAM, and there is no plan for high-memory.

* v2: [riscv: always honor the CONFIG_CMDLINE_FORCE when parsing dtb](https://lore.kernel.org/linux-riscv/PSBPR04MB399135DFC54928AB958D0638B1829@PSBPR04MB3991.apcprd04.prod.outlook.com/T/#u)

    his especially fixes the case where a device tree without the chosen node is supplied to the kernel. In such cases, 
    early_init_dt_scan would return true.

* v2: [riscv: mm: add Svnapot support](https://lore.kernel.org/linux-riscv/20220716085648.4156408-1-panqinglin2020@iscas.ac.cn/T/#t)

    Svnapot is a RISC-V extension for marking contiguous 4K pages as a non-4K page. This patch set is for using Svnapot in 
    Linux Kernel's boot process and hugetlb fs.

### 周边技术动态

#### Qemu

* v1:[RISC-V: Allow both Zmmul and M](https://lore.kernel.org/qemu-devel/20220714180033.22385-1-palmer@rivosinc.com/)

    We got to talking about how Zmmul and M interact with each other https://github.com/riscv/riscv-isa-manual/issues/869,
    and it turns out that QEMU's behavior is slightly wrong: having Zmmul and M is a legal combination, it just means that 
    the multiplication instructions are supported even when M is disabled at runtime via misa.

#### Buildroot

* v3:[Fix RV64 NOMMU and add Canaan K210 SoC support](https://lore.kernel.org/buildroot/bd4eeaff-f4ce-ecd3-8e3b-03d5a924b53a@opensource.wdc.com/)

     This series adds support for building 64-bits RISC-V NOMMU kernels (both bootable kernels and u-boot sdcard boot envronements) 
     for NOMMU RISC-V 64-bits boards.

* v1:[arch/riscv: Added support for RISC-V vector extension on the architecture menu.](https://lore.kernel.org/buildroot/CAF2ART_=XUWL8bgtdV7vUgz5SQJXVyBAAS0ixcYLVbaaA++_4w@mail.gmail.com/)

    This new setting will allow to test new toolchains already available that support the vector extension (more patches coming soon).

## 20220707：第 2 期

### 内核动态

* v5: [RISC-V: Create unique identification for SoC PMU](https://lore.kernel.org/linux-riscv/165710198557.2545727.12369986485829448520.b4-ty@kernel.org/T/#t)

    This series aims to provide matching vendor SoC with corresponded JSON bindings.Where MIMPID can vary as all impl supported the same number of 
    events, this might not be true for all future SoC however.Also added SBI firmware events pretty names, as any firmware that supports SBI PMU 
    should also support firmare events. 
    
* v7: [riscv: implement Zicbom-based CMO instructions + the t-head variant](https://lore.kernel.org/linux-riscv/20220706231536.2041855-1-heiko@sntech.de/T/#t)

    This series is based on the alternatives changes done in my svpbmt series and thus also depends on Atish's isa-extension parsing series.
    It implements using the cache-management instructions from the  Zicbom-extension to handle cache flush, etc actions on platforms needing them.

* v3: [clear riscv dtbs_check errors](https://lore.kernel.org/linux-riscv/20220606201343.514391-1-mail@conchuod.ie/T/#t)

    Couple conversions from txt to yaml here with the intent of fixing the dtbs_check warnings for riscv when building with "defconfig". 
    Atul Khare already sent patches for the gpio-line-names & cache-sets (which went awol) and will clear the remaining two errors.
    
* v4: [riscv: Optimize atomic implementation](https://lore.kernel.org/linux-riscv/YsYivaDEksXPQPaH@boqun-archlinux/T/#t)

    Here are some optimizations for riscv atomic implementation, the first three patches are normal cleanup and custom implementation without 
    relating to atomic semantics.
    
* v4: [use static key to optimize pgtable_l4_enabled](https://lore.kernel.org/linux-riscv/mhng-17913c13-57bd-42f9-9136-b4eb9632253c@palmer-mbp2014/T/#t)

    The pgtable_l4|[l5]_enabled check sits at hot code path, performance is impacted a lot. Since pgtable_l4|[l5]_enabled isn't changed after boot, 
    so static key can be used to solve the performance issue[1].
    
* v5: [arch_topology: Updates to add socket support and fix cluster ids](https://lore.kernel.org/linux-riscv/20220627165047.336669-1-sudeep.holla@arm.com/T/#t)

    This series intends to fix some discrepancies we have in the CPU topology parsing from the device tree /cpu-map node. 
    Also this diverges from the behaviour on a ACPI enabled platform. The expectation is that both DT and ACPI enabled systems 
    must present consistent view of the CPU topology.

* v5: [RISC-V: three fixup and cleanups](https://lore.kernel.org/linux-riscv/20220701120548.228261-1-xianting.tian@linux.alibaba.com/T/#t)

    Use __smp_processor_id() to avoid check the preemption context when CONFIG_DEBUG_PREEMPT enabled, as we will enter crash kernel and no return.
    
* v3: [Add PLIC support for Renesas RZ/Five SoC / Fix T-HEAD PLIC edge flow](https://lore.kernel.org/linux-riscv/92a45bf04cfe140c7605559fa3d8f4eb@kernel.org/T/#t)

    This patch series adds PLIC support for Renesas RZ/Five SoC. Since the T-HEAD C900 PLIC has the same behavior, it also applies the fix for 
    that variant. This series is an update of v2 of the RZ/Five series[0], and replaces the separate T-HEAD series[1].

* v2: [Improve instruction and CSR emulation in KVM RISC-V](https://lore.kernel.org/linux-riscv/CAAhSdy3gmnqB6La125i2hdVh6eNiwqG6saqz4RTTYF=2Gqo6cA@mail.gmail.com/T/#t)

    Currently, the instruction emulation for MMIO traps and Virtual instruction traps co-exist with general VCPU exit handling. 
    The instruction and CSR emulation will grow with upcoming SBI PMU, AIA, and Nested virtualization in KVM RISC-V. 

* v3: [irqchip: RISC-V PLIC cleanup and optimization](https://lore.kernel.org/linux-riscv/20220701202440.59059-1-samuel@sholland.org/T/#t)

    This series depends on my other series[2] making the IRQ affinity mask behavior more consistent between uniprocessor and SMP configurations.
    (The Allwinner D1 is a uniprocessor SoC containing a PLIC.)

* v2: [dt-bindings: sifive: fix dt-schema errors](https://lore.kernel.org/linux-riscv/fb861221-2e9d-7d4a-dd52-b16b3b581fd6@microchip.com/T/#t)

    The patch series fixes dt-schema validation errors that can be reproduced using the following: make ARCH=riscv defconfig; 
    make ARCH=riscv dt_binding_check dtbs_check

* v2: [riscv: Fix missing PAGE_PFN_MASK](https://lore.kernel.org/linux-riscv/d5d1a6ef-1153-272b-af9b-9f369bbdd4e5@ghiti.fr/T/#t)

    here are a bunch of functions that use the PFN from a page table entry that end up with the svpbmt upper-bits because they are 
    missing the newly introduced PAGE_PFN_MASK which leads to wrong addresses conversions and then crash: fix this by adding this mask.
    
### 周边技术动态

#### Qemu

* v7: [KVM: Add KVM_EXIT_MEMORY_FAULT exit](https://lore.kernel.org/qemu-devel/20220706082016.2603916-11-chao.p.peng@linux.intel.com/)

    This new KVM exit allows userspace to handle memory-related errors. It indicates an error happens in KVM at guest memory range (gpa, gpa+size). 
    The flags includes additional information for userspace to handle the error. 
    
* v4: [target/riscv: Add Zihintpause support ](https://lore.kernel.org/qemu-devel/20220705174933.2898412-1-daolu@rivosinc.com/)   

    This patch adds RISC-V Zihintpause support. The extension is set to be enabled by default and opcode has been added to insn32.decode.    
    
* v2: [target/riscv: Support mcycle/minstret write operation](https://lore.kernel.org/qemu-devel/20220703001234.439716-13-alistair.francis@opensource.wdc.com/)

    mcycle/minstret are actually WARL registers and can be written with any given value. With SBI PMU extension, it will be used to store a 
    initial value provided from supervisor OS.     

* v2: [target/riscv: Set env->bins in gen_exception_illegal ](https://lore.kernel.org/qemu-devel/20220703001234.439716-3-alistair.francis@opensource.wdc.com/)

    While we set env->bins when unwinding for ILLEGAL_INST, from e.g. csrrw, we weren't setting it for immediately illegal instructions.    
    
* v2: [target/riscv: Fixup MSECCFG minimum priv check](https://lore.kernel.org/qemu-devel/20220703001234.439716-14-alistair.francis@opensource.wdc.com/)

    There is nothing in the RISC-V spec that mandates version 1.12 is required for ePMP and there is currently hardware that implements ePMP 
    (a draft version though) with the 1.11 priv spec.   

#### Buildroot

* v1: [arch/riscv: Added support for RISC-V vector extension on the architecture menu.](https://lore.kernel.org/buildroot/20220704085552.3499243-2-abel@x-silicon.com/)

    This new setting will allow to test new toolchains already available that support the vector extension (more patches coming soon).

* v1: [package/llvm: Support for RISC-V on the LLVM package](https://lore.kernel.org/buildroot/20220704085552.3499243-1-abel@x-silicon.com/)

    There is a new configuration parameter added(BR2_PACKAGE_LLVM_TARGETS_TO_BUILD) for dealing with the fact that the LLVM target and 
    the architecture have different naming for RISC-V.
    
#### U-Boot

* v1: [riscv: Remove additional ifdef from code guarded by CONFIG_IS_ENABLED](https://lore.kernel.org/u-boot/f8e3ff9124195cbd957874de9a65ef79760ef5e7.1657183634.git.michal.simek@amd.com/)

    CONFIG_OF_LIBFDT is used twice for guarding the same code. It is enough to do it once 
    that's why remove additional ifdefs from arm and risc-v code.

## 20220630：第 1 期

### 内核动态

* v5: [riscv: implement Zicbom-based CMO instructions + the t-head variant](https://lore.kernel.org/linux-riscv/20220629215944.397952-1-heiko@sntech.de/T/#t)

    It implements using the cache-management instructions from the  Zicbom-extension to handle cache flush, etc actions on platforms needing them. 

* next: [riscv: lib: uaccess: fix CSR_STATUS SR_SUM bit](https://lore.kernel.org/linux-riscv/11a0698c-5726-15e8-2448-3529d2d0b098@huawei.com/T/#t)

    Since commit 5d8544e2d007 ("RISC-V: Generic library routines and assembly")and commit ebcbd75e3962 ("riscv: Fix the bug in memory access fixup code"),if __clear_user and __copy_user return from an fixup branch, CSR_STATUS SR_SUM bit will be set, it is a vulnerability, so that S-mode memory accesses to pages that are accessible by U-mode will success. Disable S-mode access to U-mode memory should clear SR_SUM bit.

* v7: [riscv: Add qspinlock support with combo style](https://lore.kernel.org/linux-riscv/20220628081707.1997728-1-guoren@kernel.org/T/#t)

    RISC-V LR/SC pairs could provide a strong/weak forward guarantee that depends on micro-architecture. And RISC-V ISA spec has given out several limitations to let hardware support strict forward guarantee (RISC-V User ISA - 8.3 Eventual Success of Store-ConditionalInstructions):We restricted the length of LR/SC loops to fit within 64 contiguous instruction bytes in the base ISA to avoid undue restrictions on instruction cache and TLB size and associativity.

* v4: [RISC-V: Create unique identification for SoC PMU](https://lore.kernel.org/linux-riscv/20220624160117.3206-1-nikita.shubin@maquefel.me/T/#t)

    This series aims to provide matching vendor SoC with corresponded JSON bindings.The ID string is proposed to be in form of MVENDORID-MARCHID-MIMPID

* v2: [Add PLIC support for Renesas RZ/Five SoC](https://lore.kernel.org/linux-riscv/20220626004326.8548-1-prabhakar.mahadev-lad.rj@bp.renesas.com/T/#t)

    This patch series adds PLIC support for Renesas RZ/Five SoC.Sending this as an RFC based on the discussion [0].This patches have been tested with I2C and DMAC interface as these blocks have EDGE interrupts.

* v1: [riscv: atomic: Clean up unnecessary acquire and release definitions](https://lore.kernel.org/linux-riscv/20220625093945.423974-1-guoren@kernel.org/T/#u)

    Clean up unnecessary xchg_acquire, xchg_release, and cmpxchg_release custom definitions, because the generic implementation is the same as the riscv custom implementation.

* v5: [rtc: microchip: Add driver for PolarFire SoC](https://lore.kernel.org/linux-riscv/165609877582.32831.3964876505949828769.b4-ty@bootlin.com/T/#t)

    This is technically a v5 of [0], although a fair bit of time has passed since then. In the meantime I upstreamed the dt-binding, which was in the v1, and this patch depends on the fixes to the dt-binding and device tree etc which landed in v5.18-rc5.

* v2: [RISC-V: fix access beyond allocated array](https://lore.kernel.org/linux-riscv/20220624135902.520748-1-geomatsi@gmail.com/T/#t)

    These patches suggest some fixes and cleanups for the handling of pmu counters. The first patch fixes access beyond the allocated pmu_ctr_list array. The second patch fixes the counters mask sent to SBI firmware: it excludes counters that were not fully specified by SBI firmware on init.

* v6: [riscv: Support qspinlock with generic headers](https://lore.kernel.org/linux-riscv/7adc9e19-7ffc-4b11-3e18-6e3a5225638f@redhat.com/T/#t)

    RISC-V LR/SC pairs could provide a strong/weak forward guarantee that depends on micro-architecture. And RISC-V ISA spec has given out several limitations to let hardware support strict forward guarantee (RISC-V User ISA - 8.3 Eventual Success of Store-Conditional Instructions)

* v1: [RISC-V: KVM: Improve ISA extension by using a bitmap](https://lore.kernel.org/linux-riscv/20220620234254.2610040-1-atishp@rivosinc.com/T/#u)

    Using a bitmap allows the ISA extension to support any number of extensions. The CONFIG one reg interface implementation is modified to support the bitmap as well.

### 周边技术动态

#### Qemu

* v9: [QEMU RISC-V nested virtualization fixes](https://lore.kernel.org/qemu-devel/CAAhSdy2iTPwqzUAhV8s97k1d4sK-bne1z-T6pg__p3xfsUrdHg@mail.gmail.com/)

    This series does fixes and improvements to have nested virtualization on QEMU RISC-V. The RISC-V nested virtualization was tested on QEMU RISC-V using Xvisor RISC-V which has required hypervisor support to run another hypervisor as Guest/VM.
    
* v9：[target/riscv: Force disable extensions if priv spec version does not match ](https://lore.kernel.org/qemu-devel/20220630061150.905174-3-apatel@ventanamicro.com/)

    We should disable extensions in riscv_cpu_realize() if minimum required priv spec version is not satisfied. This also ensures that machines with priv spec v1.11 (or lower) cannot enable H, V, and various multi-letter extensions.
    
* v10：[target/riscv: Add sscofpmf extension support](https://lore.kernel.org/qemu-devel/20220620231603.2547260-9-atishp@rivosinc.com/)

    The Sscofpmf ('Ss' for Privileged arch and Supervisor-level extensions, and 'cofpmf' for Count OverFlow and Privilege Mode Filtering) extension allows the perf to handle overflow interrupts and filtering support. 

#### Bulidroot

* v2: [board: Add Sipeed MAIX-Go board support](https://lore.kernel.org/buildroot/20220530033836.474926-11-damien.lemoal@opensource.wdc.com/)

    Add two buildroot configuration files to build a minimal Linux environment for the Sipeed MAIX Bit board.

* v1: [configs/qemu_riscv64_nommu_virt_defconfig: new defconfig](https://lore.kernel.org/buildroot/20220607190455.B6C6E869E1@busybox.osuosl.org/)

    Add RISC-V 64-bit nommu defconfig for QEMU virt machine with MMU disabled. Unlike qemu_riscv64_virt, qemu_riscv64_nommu_virt does not use OpenSBI, since the kernel is running in machine mode (M-mode).

#### U-Boot

* v3: [valgrind: Disable on Risc-V](https://lore.kernel.org/u-boot/20220527140300.682989-2-seanga2@gmail.com/)

    There are no defined instruction sequences in include/valgrind.h for Risc-V, so CONFIG_VALGRIND will do nothing on this arch.

#### Yocto

* v1: [looking to build meta-riscv based HDMI image for nezha board](https://lore.kernel.org/yocto/8d419e1-2a74-36ee-effe-c3b622ee6195@crashcourse.ca/)

    After finally unpacking my risc-v nezha SBC, i'm trying to download/build an image that will boot to a desktop with as little effort as possible. 
