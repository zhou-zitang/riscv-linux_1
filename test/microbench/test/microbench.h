#include <sstream>

#include "benchmark/benchmark.h"

// iterations optimize control
#define OPTIMIZE_LEVEL 1

#if defined(OPTIMIZE_LEVEL) && (OPTIMIZE_LEVEL == 0)
#define benchmark_DoNotOptimize_Iterations() benchmark::DoNotOptimize(state.iterations())
#else
#define benchmark_DoNotOptimize_Iterations() do { } while(0)
#endif

// clobber memory control
#define benchmark_ClobberMemory() benchmark::ClobberMemory()

// noinline control
#define BENCHMARK_NOINLINE __attribute__((noinline))

// branch extra benchmarks conttrol
#define BRANCH_EXT_BENCHMARK

// arch specific instructions
#ifndef INSN_NOP
#define INSN_NOP "nop"
#endif

#ifndef INSN_JMP
#ifdef __x86_64__
#define INSN_JMP "jmp"
#endif
#ifdef __riscv
#define INSN_JMP "jal"
#endif
#endif // INSN_JMP

// For most architectures
#ifndef INSN_JMP
#define INSN_JMP "b"
#endif
