
# Tasks

本文用于汇总 RISC-V 的相关任务和图文视频等分析成果。

大家可在相应任务后追加 `@your-gitee-id` 来认领任务，也可以提交 PR 新增或修订任务。

相关图文或直播回放成果在发布后将于第一时间更新进来。

## QuickStart

* RISC-V Linux Analyse Howto
    * [如何分析 Linux 内核 RISC-V 架构相关代码](https://tinylab.org/riscv-linux-quickstart/) @falcon

* RISC-V Linux Distributions
    * [两分钟内极速体验 RISC-V Linux 系统发行版](https://tinylab.org/riscv-linux-distros/) @falcon
    * [5 秒内跨架构运行 RISC-V Ubuntu 22.04 + xfce4 桌面系统](https://tinylab.org/run-riscv-ubuntu-over-x86/) @falcon

* Pull Request Howto
    * [直播回放：提交 PR 须知 —— 中英文排版等](https://www.cctalk.com/v/16484083661860) @falcon
    * [直播回放：RISC-V Linux技术调研暑期实习启动会暨入职培训](https://www.bilibili.com/video/BV1GV4y1H77p) @falcon
    * [直播回放：RISC-V Linux技术调研暑期实习宣讲会](https://www.cctalk.net/v/16574310111172) @falcon

* Linux Upstream Howto
    * [直播回放：开放实习与兼职岗位并启动 Linux Upstream计划](https://www.cctalk.com/v/16551875274459) @falcon

## Experiments

* RISC-V Assembly
    * [视频演示：快速上手 RISC-V 汇编语言实验](https://www.cctalk.com/v/16484116944868) @tinylab

* RISC-V OS Basics
    * [视频演示：无门槛开展 RVOS 课程实验](https://www.cctalk.com/v/16484116944869) @tinylab

* RISC-V Linux Kernel Development
    * [视频演示：极速开展 RISC-V Linux v5.17 内核实验](https://www.cctalk.com/v/16484095223067) @tinylab

## Hardware

* RISC-V CPU Design
    * [大学生 RISC-V 处理器设计实践](https://www.bilibili.com/video/BV1L54y1o7Fs)
    * 从零开始设计 RISC-V CPU @Fajie.WangNiXi

* RISC-V Board Design
    * [开源之夏2022 - RISC-V 硬件产品开发实录](https://gitee.com/tinylab/cloud-lab/issues/I56CKU)
    * [直播回放：I2C & SPI 用法](https://www.cctalk.net/v/16586599461554) @taotieren 
    * [直播回放：原理图简介 & SDIO 用法](https://www.cctalk.net/v/16592486764588) @taotieren 

* RISC-V Board Usage
    * D1 Board
        * [D1-H 开发板入门](https://tinylab.org/nezha-d1-quickstart/) @iosdevlog
        * [为哪吒 D1 开发板安装 ArchLinux RISC-V rootfs](https://tinylab.org/nezha-d1-archlinux/) @tieren
    * Longan Nano
        * [在 Linux 下制作 rv-link 调试器](https://tinylab.org/rv-link-debugger/) @tieren

## Specs

* ISA
    * [RISC-V ISA 简介](https://tinylab.org/riscv-isa-intro), [幻灯](https://gitee.com/tinylab/riscv-linux/raw/master/ppt/riscv-isa.pptx), [直播回放](https://www.cctalk.com/v/16484079493695) @iOSDevLog 
    * [RISC-V 特权指令](https://tinylab.org/riscv-privileged/) @pingbo

* psABI
* SBI
* Extensions

## Emulation & Simulation

* rv8
* Spike
* Qemu
* [JuiceVm](https://github.com/juiceRv/JuiceVm)
* Other
    * [Writing a simple RISC-V emulator in plain C (Base integer, multiplication and csr instructions)](https://fmash16.github.io/content/posts/riscv-emulator-in-c.html) @yjmstr 

## Basic support

* Build Infrastructure
* User-facing API
* Generic library routines and assembly @郭天佑TanyoKwok
    * FPU
    * non FPU
    * [non M-extension](https://lore.kernel.org/linux-riscv/20220402101801.GA9428@amd/T/#t)
    * futex

## Bootloader

* BBL (Berkeley Boot Loader)
* OpenSBI
    * [OpenSBI 快速上手](https://tinylab.org/riscv-opensbi-quickstart/) @falcon
* RustSBI
    * RISC-V 引导程序环境：应用&规范，[幻灯](https://gitee.com/tinylab/riscv-linux/raw/master/ppt/riscv-rustsbi.pdf)，[直播回放](https://www.cctalk.com/v/16495466863205) @luojia65
* U-Boot
* UEFI
    * [RISC-V UEFI 架构支持详解，第 1 部分 - OpenSBI/U-Boot/UEFI 简介](https://tinylab.org/riscv-uefi-part1/) @Jacob

## Boot, Init & Shutdown

* Booting
    * RISC-V Linux 内核启动流程分析，[直播回放](https://www.cctalk.com/v/16520639214118) @通天塔
    * [RISC-V Linux 启动流程分析](https://tinylab.org/riscv-linux-startup/) @通天塔 

* DTS @iOSDevLog 
    * [直播回放：RISC-V Linux 设备树语法与格式详解](https://www.cctalk.net/v/16598634312907) @iOSDevLog 

* UEFI support @jiangbo.jacob
* Compressed images
* Early Boot
* earlycon @iosdevlog
* Soc Init
* setup_arch @marycarry
* poweroff
* printk
    * 内核调试基础设施之 printk 初探，[直播回放](https://www.cctalk.com/v/16551876033662) @iOSDevLog 

## Memory Management

* Paging and MMU
    * [RISC-V MMU 概述](https://tinylab.org/riscv-mmu/), [直播回放](https://www.cctalk.com/v/16477623213273) @pwl999

* memblock @tjytimi
* setup_vm @davidlamb
* fixed mapping @tjytimi
* ioremap @tjytimi
* buddy @tjytimi
* per_cpu_cache @tjytimi
* vmalloc @tjytimi
* process's address space @tjytimi
* mmap @tjytimi
* page_fault @tjytimi
* Barriers @cursor_risc-v
* Sparsemem @Jack Y.
* CMA 
* DMA 
* Hugetlbfs 
* memtest
* NUMA
* nommu
* msharefs

## Device, IRQs, Traps and the SBI

* SBI
* Device
* IRQs @通天塔 @williamsun0122
* Traps
* swiotlb

## Timers

* Timers
    * [RISC-V timer 在 Linux 中的实现](https://tinylab.org/riscv-timer/), [直播回放](https://www.cctalk.net/v/16566997322462) @yuliao

* swtimer @kyrie.xi
* udelay

## Synchronization

* Atomics
    * [RISC-V 原子指令与用法介绍](https://tinylab.org/riscv-atomics/), [直播回放](https://www.cctalk.com/v/16489499392383) @pingbo

* [Generic Ticket Spinlock](https://lore.kernel.org/linux-riscv/11364105.8ZH9dyz9j6@diego/T/#t)
* [Restartable Sequence (RSEQ)](https://lore.kernel.org/linux-riscv/20220308083253.12285-1-vincent.chen@sifive.com/T/#t)
* [Qspinlock](https://lore.kernel.org/linux-riscv/CAJF2gTT9YHgTzPaBN4ekYS7UcBOj_9k9xEcrsoXgW6PCZc8x3Q@mail.gmail.com/T/#t)

## Multi-core

* SMP support @user_11186799
* IPI
* Hotplug
* CPUIdle

## Scheduling

* Multi-tasking @Jack Y.
* Context switch @Jack Y.
    * RISC-V Linux 内核调度详解，[直播回放](https://www.cctalk.com/v/16558003571816)  @Jack Y.
    * [RISC-V Linux 上下文切换分析](https://tinylab.org/riscv-context-switch/)  @Jack Y.
    * [RISC-V Linux __schedule() 分析](https://tinylab.org/riscv-linux-schedule/)  @Jack Y.

* Task implementation @tjytimi 
    * [RISC-V Linux 进程创建与执行流程代码分析](https://tinylab.org/riscv-task-implementation/)  @tjytimi 
    * [RISC-V 架构下内核线程返回函数探究](https://tinylab.org/riscv-kthread-ret/)  @tjytimi 

* EAS

## User-space Support

* Syscall
    * RISC-V Linux 系统调用详解，[直播回放](https://www.cctalk.com/v/16562391483157) @envestcc

* ELF support
* module implementation
* Multi-threads
* binfmt_flat

## Power Management

* Suspend To RAM
* Suspend To Disk
* Wakeup Source Support
* Clock Gating
* Regulator

## Thermal Management

## Tracing

* Stacktrace
    * [RISC-V Linux Stacktrace 详解](https://tinylab.org/riscv-stacktrace/), [直播回放](https://www.cctalk.com/v/16525972863665) @zhao305149619

* Tracepoint
* Ftrace @sugarfillet
* [Fprobe](https://www.kernel.org/doc/html/latest/trace/fprobe.html)
* Kprobes @juliwang
* Uprobes
* [User Events](https://www.kernel.org/doc/html/latest/trace/user_events.html)
* eBPF @jams_liu

## Debugging

* Ptrace
* Crash
* KGDB
* Kexec & Kdump

## Detection

* KCSAN
* KASAN
* KMSAN
* KFENCE
    * [Linux Kfence 详解](https://tinylab.org/riscv-linux-kfence/)  @pwl999 
* Kmemleak

## Profiling

* Perf
* PMU (Performance Monitor Unit)
* kcov

## Tuning

* vdso
* Jump Label
    * [RISC-V Linux jump_label 详解，第 1 部分：技术背景](https://tinylab.org/riscv-jump-label-part1/) @falcon
    * [RISC-V Linux jump_label 详解，第 2 部分：指令编码](https://tinylab.org/riscv-jump-label-part2/), [直播回放](https://www.cctalk.com/v/16501801841966) @falcon
    * [RISC-V jump_label 详解，第 3 部分：核心实现](https://tinylab.org/riscv-jump-label-part3/) @falcon
    * [RISC-V jump_label 详解，第 4 部分：运行时代码改写](https://tinylab.org/riscv-jump-label-part4/) @falcon

* Static call

## Security

* Stackprotector
* OP-TEE for RISC-V
* PengLai for RISC-V

## Virtulization

* [KVM](https://lore.kernel.org/linux-riscv/CAAhSdy3JwLyOoNOubAS2VusNdj6O-CJJNSs+mM8+NvKErtAdmw@mail.gmail.com/T/#u) @walimis 
    * [直播回放：GPU Virtulization](https://www.cctalk.net/v/16574325601701)  @walimis 

## Fast boot

* XIP

## Real Time

* Preemption

## Benchmarking

* Microbench
    * [RISC-V 处理器指令级性能评测尝试](https://tinylab.org/riscv-perf-benchmark/)
    * [开源之夏2022 - 处理器指令级 Microbench 开发实录](https://gitee.com/tinylab/cloud-lab/issues/I57CM0)
* [martinus/nanobench](https://github.com/martinus/nanobench)
* [andreas-abel/nanoBench](https://github.com/andreas-abel/nanoBench)

## Testing

* Autotest frameworks

## Latest development

* [News](https://gitee.com/tinylab/riscv-linux/tree/master/news)，已新建独立的 News 发布页