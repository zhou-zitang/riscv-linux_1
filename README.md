
# RISC-V Linux 内核剖析

该项目由 [泰晓科技](https://tinylab.org) 技术社区发起，致力于剖析 Linux 内核的 RISC-V 架构相关支持。

## 任务安排

相关任务整理在 [plan](https://gitee.com/tinylab/riscv-linux/tree/master/plan)，已经整理了任务列表，会不断迭代和更新，大家可以认领自己感兴趣的模块。

**认领任务**

认领过程很简单：

1. 添加微信号 tinylab，并介绍相关从业背景，可附上以往发表过的文章或博客链接。
2. 确认后，请在 [plan/README.md](https://gitee.com/tinylab/riscv-linux/blob/master/plan/README.md) 挑选 1 个感兴趣的 task，点击 “编辑”，在任务后加上自己的 Gitee ID，提交即可。
3. 提交认领 PR 后会邀请进入协作群组。

**提交成果**

认领后请自行查找资料、阅读源码并撰写分析文章，完成质量很高的文章可以提交 PR 到 [articles](https://gitee.com/tinylab/riscv-linux/tree/master/articles) 目录，提交 PR 前请查阅 [articles/README.md](https://gitee.com/tinylab/riscv-linux/blob/master/articles/README.md) 的相关约定。

之后社区会择优发布到社区网站、知乎专栏等渠道，内容协议默认为：[CC BY-NC-ND 4.0 协议](http://creativecommons.org/licenses/by-nc-nd/4.0/)。另外，也会择优邀请开展技术直播分享。

**相关福利**

为支持该活动顺利开展并鼓励大家踊跃参与，提交并 merge 首笔文章 PR 后，社区会赠送 3 份小福利：

* 一本 Linux 或周边图书（数量有限，发完为止，欢迎赞助手头已看完的闲置 Linux 内核图书）
* [Linux Lab](https://tinylab.org/linux-lab) 或即插即跑版 [Linux Lab Disk](https://tinylab.org/linux-lab-disk) 的 riscv64/virt 虚拟开发板，方便分析源码、开展实验
    * 提交任务认领 PR 并被 merge 后即可申请
* [Linux 知识星球](https://t.zsxq.com/uB2vJyF) 年度会员，方便一起速记学习笔记

## 实验环境

### Linux 内核与 QEMU 开发

[Linux Lab](https://gitee.com/tinylab/linux-lab) 开源项目已经提供了完整的 RISC-V 32/64 Linux 内核和 QEMU 开发支持，本次活动的相关文章与视频的实验与演示部分统一采用 Linux Lab。

大家可参考 [Linux Lab 中文用户手册](https://tinylab.org/pdfs/linux-lab-v1.0-manual-zh.pdf) 和 [Linux Lab 公开课](https://www.cctalk.com/m/group/88948325) 自行搭建环境。也可以直接使用免安装即插即跑的泰晓 Linux 实验盘，其用法、特性和功能请参考 [Linux Lab Disk 用法](https://tinylab.org/linux-lab-disk)，如需选购可在某宝检索“泰晓 Linux”关键字。

认领任务并成功提交一篇分析成果到 [articles](https://gitee.com/tinylab/riscv-linux/tree/master/articles) 后可免费申请开通 `riscv32/virt` 和 `riscv64/virt` 虚拟开发板，如果选购 Linux Lab Disk，可直接申请帮忙内置进去。

### RISC-V 系统与软件开发

社区近日开发了一套 [RISC-V Lab](https://gitee.com/tinylab/riscv-lab)，它允许在 X86_64 主机上直接运行一个 RISC-V Linux 桌面系统，5 秒钟内即可启动。其基础系统目前采用 Ubuntu 22.04，已支持 lxqt, xfce 桌面，并内置了 gcc, gdb 等开发工具，方便在没有 RISC-V 硬件的情况下直接开展 RISC-V 系统与软件开发。

其运行和登陆用法跟 Linux Lab 类似，也可以在泰晓 Linux 实验盘下运行。

## 相关资料

相关资料整理在 [refs](https://gitee.com/tinylab/riscv-linux/tree/master/refs)，含各种 Spec、移植文档、思维导图、视频课程等。

欢迎大家增补相关资料，觉得不错的内容可以提交 PR。为了避免知识产权风险，仅接受资料 URL，不接受直接上传文件。

## 会议管理

本次活动计划每周六晚上 8:00 - 9:30 组织活动会议，前半个小时为交流环节，分享活动进展，探讨技术问题，后半个小时为技术直播分享环节。

会议管理见 [meeting](https://gitee.com/tinylab/riscv-linux/tree/master/meeting) 目录。

## 成果发布

相应的成果会以文字、视频或者直播的形式开展分享，以下是可能发布的渠道。

* 文字内容
    * 该仓库 [articles](https://gitee.com/tinylab/riscv-linux/tree/master/articles) 目录
    * [社区网站](https://tinylab.org) --> [投递地址](https://gitee.com/tinylab/tinylab.org)
    * [知乎专栏](https://www.zhihu.com/column/tinylab)
    * [知识星球](https://t.zsxq.com/uB2vJyF)

* 视频内容
    * [泰晓学院](https://www.cctalk.com/m/group/90251209)
    * [B 站频道](https://space.bilibili.com/687228362/channel/collectiondetail?sid=273934)
    * [知乎专栏](https://www.zhihu.com/column/tinylab)

* 注意事项
    * 为保持活动内容的整体性，也为了方便成系列地连载，并确保内容的质量，[泰晓科技](https://tinylab.org) 保留在上述渠道优先发表文章或视频等活动成果的权力
    * 相关文章的作者或译者们在泰晓科技发表后可以在其他自有渠道转发，但是需保留原有的统一文件头信息，以尊重活动方、作者、校订者等多方的劳动成果

## 版权说明

本次活动的所有文字、视频等成果归 [泰晓科技](https://tinylab.org) 及其作者或译者共同所有，以 [CC BY-NC-ND 4.0 协议](https://creativecommons.org/licenses/by-nc-nd/4.0/) 对外公开发表，任何其他个人、企业、机构、组织等未经 [泰晓科技](https://tinylab.org) 的授权不得转发、不得用于商业用途，不得篡改重新发表等。

为了推进活动取得更好的成效，[泰晓科技](https://tinylab.org) 保留组织并结集出版相关成果等权力。

## 联系我们

该项目持续欢迎相关从业人员参与协作。

* 微信：tinylab
* 公号：泰晓科技
* 邮件：contact@tinylab.org
* 网站：<https://tinylab.org>

## 致谢

感谢中科院软件所 PLCT 实验室对该活动的大力支持。

![ISCAS](refs/logos/iscas.png)
