// 采用基本数据类型自带的 mux() 函数实现多选器
val bitwiseSelect = UInt(2 bits)
val bitwiseResult = bitwiseSelect.mux(
  0 -> (io.src0 & io.src1),
  1 -> (io.src0 | io.src1),
  2 -> (io.src0 ^ io.src1),
  3 -> (io.src0)
)

// 采用三目运算符方式实现多选器
val bitwiseResult_2 = (bitwiseSelect === 0) ? (io.src0 & io.src1) |
											(bitwiseSelect === 1) ? (io.src0 | io.src1) |
											(bitwiseSelect === 2) ? (io.src0 ^ io.src1) |
											(io.src0)
